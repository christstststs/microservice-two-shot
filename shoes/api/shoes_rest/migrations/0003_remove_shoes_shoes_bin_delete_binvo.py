# Generated by Django 4.0.3 on 2022-09-08 23:03

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0002_binvo_rename_manufacture_shoes_manufacturer_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shoes',
            name='shoes_bin',
        ),
        migrations.DeleteModel(
            name='BinVO',
        ),
    ]
