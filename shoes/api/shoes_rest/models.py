from django.db import models
from django.urls import reverse

# Create your models here.

class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href = models.CharField(max_length=200, unique=True)

class Shoes(models.Model):
    manufacturer = models.CharField(max_length=100, unique=True)
    model_name = models.CharField(max_length=100, unique=True)
    color = models.CharField(max_length=100, unique=True)
    picture_url = models.URLField(null=True)
    shoes_bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
        null=True
    )



    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"pk": self.pk})

    def __str__(self):
        return self.model_name



