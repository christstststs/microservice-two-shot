import React, { useState } from "react";

function ShoesList(props) {
    const [currentShoe, setCurrentShoe] = useState(props.shoes[0]);

    const [currentShoeBin, setCurrentShoeBin] = useState();

    const getShoeDetail = async () => {
        const url = `http://localhost:8080/api/shoes/${currentShoe.id}/`;
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCurrentShoeBin(data.bin.closet_name);
        }
    }

    const deleteShoe = async () => {
        fetch(`http://localhost:8080/api/shoes/${currentShoe.id}/`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        })
        window.bin.reload();
    }

    const handleClick = async (Shoe) => {
        console.log("setting current Shoe", Shoe);
        setCurrentShoe(Shoe);
        getShoeDetail();
    };

    return (
        <>
            <table className="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>manufacturer</th>
                        <th>model_name</th>
                        <th>Color</th>
                        <th>Remove</th>
                    </tr>
                </thead>
                <tbody>
                    {props.shoes.map((Shoe) => {
                        return (
                            <tr
                                key={Shoe.id}
               
                            >
                                <td>{Shoe.manufacturer}</td>
                                <td>{Shoe.model_name}</td>
                                <td>{Shoe.color}</td>
                                <td>
                                    <button onClick={() => handleClick(Shoe)}  data-bs-toggle="modal"  data-bs-target="#ShoeModal" type="button">
                                        Delete
                                    </button>
  
                                
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <div
                className="modal fade"
                id="ShoeModal"
                tabIndex="-1"
                aria-labelledby="ShoeModalLabel"
                aria-hidden="true"
            >
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button
                                type="button"
                                className="btn-close"
                                data-bs-dismiss="modal"
                                aria-label="Close"
                            ></button>
                        </div>
                        {currentShoe && (
                            <>
                                <div className="modal-body">
                                    <img
                                        src={currentShoe.picture_url}
                                        className="rounded mx-auto d-block img-fluid"
                                    />
                                    <table className="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Manufacturer</th>
                                                <th>model Name</th>
                                                <th>Color</th>
                                                <th>Remove</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{currentShoe.manufacturer}</td>
                                                <td>{currentShoe.model_name}</td>
                                                <td>{currentShoe.color}</td>
                                                <td>{currentShoeBin}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div className="modal-footer">
                                        <button 
                                            onClick={() => deleteShoe()}
                                            type="button"
                                            className="btn btn-secondary"
                                            data-bs-dismiss="modal"
                                        >
                                            Are you sure?


                                        </button>
                                    </div>
                                </div>
                            </>
                        )}
                    </div>
                </div>
            </div>
        </>
    );
}

export default ShoesList;



