import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import HatForm from './HatForm';
import ShoesForm from './ShoesForm';
import ShoesList from './ShoesList';

function App(props) {
  if (props.shoes === undefined && props.hats === undefined) {
    return null
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          {/* <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatList hats={props.hats} />} />
            <Route path="new" element={<HatForm />} />
          </Route> */}
          <Route path="shoes">
            <Route path="new" element={<ShoesForm />} />
            <Route path="" element={<ShoesList shoes={props.shoes} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
