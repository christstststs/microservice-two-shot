import React from 'react';

class ShoesForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            manufacturer: "",
            model_name: "",
            color: "",
            picture_url: "",
            shoes_bins: [],
        }

        this.handleSubmit = this.handleSubmit.bind(this); 
        this.handleChangeManufacturer = this.handleChangeManufacturer.bind(this);
        this.handleChangeModelName = this.handleChangeModelName.bind(this);
        this.handleChangeColor = this.handleChangeColor.bind(this);
        this.handleChangePictureUrl = this.handleChangePictureUrl.bind(this);
        this.handleChangeShoesBins = this.handleChangeShoesBins.bind(this);
          
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            //console.log(data)
            this.setState({shoes_bins: data.bins});

            };
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.shoes_bins;
        //.log(data);

        const shoesUrl = "http://localhost:8080/api/shoes/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok) {
            const newShoes= await response.json();
             
            
            const cleared = {
                manufacturer: "",
                model_name: "",
                color: "",
                picture_url: "",
                shoes_bin: "",

            };
            this.setState(cleared);
        }
        
    }


    handleChangeManufacturer(event) {
        const value = event.target.value;
        this.setState({manufacturer: value})
        //console.log(value)
    }
    handleChangeModelName(event) {
        const value = event.target.value;
        this.setState({model_name: value})
      } 
    handleChangeColor(event) {
        const value = event.target.value;
        this.setState({color: value})
      } 
    handleChangePictureUrl(event) {
        const value = event.target.value;
        this.setState({picture_url: value})
      }
    handleChangeShoesBins(event) {
        const value = event.target.value;
        this.setState({shoes_bin: value})
      }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            //console.log(data)
            this.setState({shoes_bins: data.bins});

            

        };
    }

    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Store new shoes</h1>
                        <form onSubmit={this.handleSubmit} id="create-shoes-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.manufacturer} onChange={this.handleChangeManufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                                <label htmlFor="manufacturer">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.model_name} onChange={this.handleChangeModelName} placeholder="Model name" required type="text" name="model_name" id="model_name" className="form-control" />
                                <label htmlFor="model_name">Model name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.color} onChange={this.handleChangeColor} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="inputGroup-sizing-default">Picture URL</span>
                                </div>
                                    <input className="form-control" aria-label="Default" aria-describedby="inputGroup-sizing-default" onChange={this.handleChangePictureUrl} type="text" name="picture_url" id="picture_url" value={this.state.picture_url}/>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.shoes_bin} onChange={this.handleChangeShoesBins} required id="shoes_bin" name="shoes_bin" className="form-select">
                                    <option value="">Choose a bin</option>
                                    {this.state.shoes_bins.map(shoes_bin => {
                                        return (
                                            <option key={shoes_bin.href} value={shoes_bin.href}>
                                                {shoes_bin.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Store</button>
                        </form>
                        {/* <div className="alert alert-success d-none mb-0" id="success-message">
                            Congratulations! You stored new shoes!
                        </div> */}
                    </div>
                </div>
            </div>
        )
    }

}

export default ShoesForm;
