from django.contrib import admin
from hats_rest.models import Hat, LocationVO

# Register your models here.
admin.site.register(Hat)
admin.site.register(LocationVO)

